package com.example.diagnalprogrammingtest.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.diagnalprogrammingtest.R
import com.example.diagnalprogrammingtest.custome_array.MovieList
import kotlinx.android.synthetic.main.adapter_home_movie_list.view.*

class MovieAdapter (context: Context, var movieList: ArrayList<MovieList>) : BaseAdapter() {
    var context: Context? = context

    override fun getCount(): Int {
        return movieList.size
    }

    override fun getItem(position: Int): Any {
        return movieList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val food = this.movieList[position]

        val inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val movieView = inflator.inflate(R.layout.adapter_home_movie_list, null)
        movieView.imgFood.setImageResource(food.image!!)
        movieView.tvName.text = food.name!!

        return movieView
    }
}