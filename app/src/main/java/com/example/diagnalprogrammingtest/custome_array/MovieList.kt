package com.example.diagnalprogrammingtest.custome_array

class MovieList {
    var name: String? = null
    var image: Int? = null

    constructor(name: String, image: Int) {
        this.name = name
        this.image = image
    }
}