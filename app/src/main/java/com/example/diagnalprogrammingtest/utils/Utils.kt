package com.example.diagnalprogrammingtest.utils

import android.content.Context
import android.net.ConnectivityManager

class Utils {

    fun isConnected(mContext: Context): Boolean {
        val cm = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.isActiveNetworkMetered()
    }

}