package com.example.diagnalprogrammingtest.utils.custome_ui

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import com.example.diagnalprogrammingtest.R

class CustomeTextViewFontBold : androidx.appcompat.widget.AppCompatTextView {
    constructor(context: Context) : super(context) {
        val face = Typeface.createFromAsset(
            context.assets,
            context.resources.getString(R.string.TitilliumWeb_Bold)
        )
        this.setTypeface(face)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        val face = Typeface.createFromAsset(
            context.assets,
            context.resources.getString(R.string.TitilliumWeb_Bold)
        )
        this.setTypeface(face)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int
    ) : super(context, attrs, defStyle) {
        val face = Typeface.createFromAsset(
            context.assets,
            context.resources.getString(R.string.TitilliumWeb_Bold)
        )
        this.setTypeface(face)
    }
}