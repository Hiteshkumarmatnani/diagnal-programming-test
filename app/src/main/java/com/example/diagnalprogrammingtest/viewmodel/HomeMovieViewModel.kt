package com.example.diagnalprogrammingtest.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.diagnalprogrammingtest.custome_array.MovieList
import com.example.diagnalprogrammingtest.repository.HomeMovieRepositories

class HomeMovieViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var mHomeMovieRepositories: HomeMovieRepositories

    fun int()
    {
        mHomeMovieRepositories = HomeMovieRepositories().getInstance()!!
    }

    fun getAllEmployee(): LiveData<List<MovieList>> {
        return mHomeMovieRepositories.fetchMovieListFromAPI(getApplication())
    }
}