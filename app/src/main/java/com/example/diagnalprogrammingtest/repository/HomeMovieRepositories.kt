package com.example.diagnalprogrammingtest.repository

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.diagnalprogrammingtest.custome_array.MovieList
import com.example.diagnalprogrammingtest.utils.Utils
import org.json.JSONArray
import org.json.JSONObject

class HomeMovieRepositories {

    private var instance: HomeMovieRepositories? = null
    private val mutableLiveData: MutableLiveData<List<MovieList>> =
        MutableLiveData()
    private var pageIndex: Int = 0

    fun getInstance(): HomeMovieRepositories? {
        if (instance == null) {
            instance = this

        }
        return instance
    }

    fun fetchMovieListFromAPI(mContext: Context): MutableLiveData<List<MovieList>>
    {
        if (!Utils().isConnected(mContext))
        {
            val movieListLiveData = mutableListOf<MovieList>()
            var file_name = ""
            if (pageIndex == 0) {
                file_name = "CONTENTLISTINGPAGE-PAGE1.json"
            } else if (pageIndex == 1) {
                file_name = "CONTENTLISTINGPAGE-PAGE2.json"
            } else if (pageIndex == 2){
                file_name = "CONTENTLISTINGPAGE-PAGE3.json"
            }

            if (file_name.isNotEmpty()){
                val json_string = mContext.assets.open(file_name).bufferedReader().use{
                    it.readText()
                }

                try {
                    val jsonObject = JSONObject(json_string)
                    val pageJsonObject = JSONObject(jsonObject.toString()).getString("page")
                    val contentItemsJsonObject = JSONObject(pageJsonObject.toString()).getString("content-items")
                    val contentJsonObject = JSONObject(contentItemsJsonObject.toString()).getString("content")

                    val jsonArray = JSONArray(contentJsonObject.toString())
                    for (contentArray in 0 until jsonArray.length()){
                        val jsonObject = jsonArray.getJSONObject(contentArray)
                        val resID = mContext.resources.getIdentifier(jsonObject.getString("poster-image").toString().replace(".jpg", ""),
                            "drawable", mContext.packageName)
                        movieListLiveData.add(MovieList(jsonObject.getString("name"), resID))
                    }
                    pageIndex++
                } catch (e: Exception){
                    e.printStackTrace()
                }
            }

            mutableLiveData.value = movieListLiveData
        } else {
            Toast.makeText(mContext, "Oops! no internet", Toast.LENGTH_LONG).show()
        }
        return mutableLiveData
    }

}