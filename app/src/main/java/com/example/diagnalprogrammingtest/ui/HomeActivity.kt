package com.example.diagnalprogrammingtest.ui

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.diagnalprogrammingtest.R
import com.example.diagnalprogrammingtest.custome_array.MovieList
import com.example.diagnalprogrammingtest.view.MovieAdapter
import com.example.diagnalprogrammingtest.viewmodel.HomeMovieViewModel
import java.util.*
import kotlin.collections.ArrayList

class HomeActivity : AppCompatActivity() {

    var adapter: MovieAdapter? = null
    var movieList = ArrayList<MovieList>()
    var searchMovieList = ArrayList<MovieList>()
    private var mHomeMovieViewModel: HomeMovieViewModel? = null
    private lateinit var noInternet_rel: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        getSupportActionBar()!!.hide()

        val gridView            =   findViewById<GridView>(R.id.gridView)
        val searchMovie         =   findViewById<SearchView>(R.id.searchMovie)
        val searchView_ln       =   findViewById<LinearLayout>(R.id.searchView_ln)
        val search_rel          =   findViewById<RelativeLayout>(R.id.search_rel)
        val actionBarView_rel   =   findViewById<RelativeLayout>(R.id.actionBarView_rel)
        val retry_rel           =   findViewById<RelativeLayout>(R.id.retry_rel)
        noInternet_rel          =   findViewById<RelativeLayout>(R.id.noInternet_rel)

        val editText = searchMovie.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        editText.setTextColor(Color.WHITE)

        val closeButton = searchMovie.findViewById<ImageView>(androidx.appcompat.R.id.search_close_btn)

        adapter = MovieAdapter(this, searchMovieList)

        gridView.adapter = adapter

        mHomeMovieViewModel = ViewModelProvider(this).get(HomeMovieViewModel::class.java)
        mHomeMovieViewModel!!.int()
        loadData()

        search_rel.setOnClickListener { v ->
            actionBarView_rel.visibility = View.GONE
            searchView_ln.visibility = View.VISIBLE
        }

        closeButton.setOnClickListener {
            // Manage this event.
            searchView_ln.visibility = View.GONE
            actionBarView_rel.visibility = View.VISIBLE
            searchMovieList.clear()
            searchMovieList.addAll(movieList)
            adapter!!.notifyDataSetChanged()
        }

        searchMovie.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (query.isNotEmpty()) {
                    searchMovieList.clear()
                    for (item in movieList) {
                        if (item.name!!.toLowerCase(Locale.ROOT).contains(query.toLowerCase(Locale.ROOT))){
                            searchMovieList.add(item)
                        }
                    }

                    adapter!!.notifyDataSetChanged()
                } else if (query.isEmpty()){
                    searchMovieList.clear()
                    searchMovieList.addAll(movieList)
                    adapter!!.notifyDataSetChanged()
                }
                return false
            }

            override fun onQueryTextChange(searchText: String): Boolean {

                if (searchText.length >= 3) {
                    searchMovieList.clear()
                    for (item in movieList) {
                        if (item.name!!.toLowerCase(Locale.ROOT).contains(searchText.toLowerCase(
                                Locale.ROOT))
                        ) {
                            searchMovieList.add(item)
                        }
                    }

                    adapter!!.notifyDataSetChanged()
                } else if (searchText.isEmpty()){
                    searchMovieList.clear()
                    searchMovieList.addAll(movieList)
                    adapter!!.notifyDataSetChanged()
                }
                return false
            }
        })

        gridView.setOnScrollListener(object: AbsListView.OnScrollListener {
            override fun onScroll(view: AbsListView?, firstVisibleItem: Int, visibleItemCount: Int, totalItemCount: Int) {
                if (totalItemCount > 0) {
                    val lastVisibleItem = firstVisibleItem + visibleItemCount
                    if (!isLoading && hasMoreItems && lastVisibleItem == totalItemCount) {
                        isLoading = true
                        //load more items--
                        loadData()
                    }
                }
            }
            override fun onScrollStateChanged(view: AbsListView?, state: Int) {
                //TODO: add some logic if needed, but no logic needed for this task
            }
        })

        retry_rel.setOnClickListener { loadData() }

    }

    var isLoading: Boolean = false
    var hasMoreItems: Boolean = true
    fun loadData() {

        mHomeMovieViewModel!!.getAllEmployee()
            .observe(this, Observer<List<MovieList>> {
                t: List<MovieList>? ->
                if (t != null) {
                    if (t.isEmpty() && movieList.size > 0){
                        hasMoreItems = false
                    }
                }

                isLoading = false
                t?.let { movieList.addAll(it) }
                searchMovieList.addAll(movieList)
                adapter!!.notifyDataSetChanged()
            })
    }

}

